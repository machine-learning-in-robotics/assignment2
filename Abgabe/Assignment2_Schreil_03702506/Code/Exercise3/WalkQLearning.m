function[policy] = WalkQLearning(s)

% Implementaion of Q-Learning algorithm with epsilon-greedy strategy. The
% Exploration-Exploitation Tradeoff is given by the epsilon-greedy policy, 
% which means that the robot will act greedily with a probabiliity of 
% (1-epsilon) and choosing a random policiy with a probability of epsilon

% Actions and their effect on humanoid robot 
%   Action  |           Effect
%   --------|------------------------------- 
%      1    | Move right lef up or down      
%      2    | Move right leg back or forward 
%      3    | Move left leg up or down 
%      4    | Move left leg back or forward 

% learning parameters
gamma = 0.4;    % discount factor  
alpha = 0.2;    % learning rate    
epsilon = 0.3;  % exploration prob. (1-epsilon=exploit / epsilon=explore)

% Define a maximum Number of iterations 
max_iter = 5000; 
% init Q(s,a) with zeros
Q = zeros(16,4);
% init starting State 
state = s; 

for iter = 1:max_iter
    % choose either Exploration or Exploitation strategy 
    if rand < epsilon  
        % choose random action
        a = ceil(rand*4); 
    else
        % pick optimal action 'a' acording to greedy stratecy by choosing
        % the action which gives the highest reward 
        %[~, a] = max(Q(state, :)); 
        % -> with this approach i got problems by the pure greedy policy, 
        %    the zero initialisation of Q leads the algorithm to get stuck
        %    in action 1
        r = -inf;
        for aa = 1:4
            [~, reward] = SimulateRobot(state, aa);
            if reward > r
                a = aa;
            end
        end
        
    end
    % simualte Robot with actual State and choosen action 
    [new_state, reward] = SimulateRobot(state, a);
    % Update Value-Action function
    Q(state, a) = Q(state, a) + alpha*(reward + gamma*max(Q(new_state, :)) - Q(state,a));
    
    % Update State
    state = new_state;
end 

% calcualte optimal policy by switching to greedy policy 
for ss = 1:16
    [~, policy(ss,1)] = max(Q(ss, :));
end 

state_seq(1) = s;
for i=2:16
    [state_seq(i), ~] = SimulateRobot(state_seq(i-1), policy(state_seq(i-1)));
end 
walkshow(state_seq)
title(sprintf("Learned policy with Q-learning, inital state: %d", s));