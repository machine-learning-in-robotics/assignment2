function [policy, iter]=WalkPolicyIteration(s)

% Apply Policy Iteration for learing the gait sequence 

% Actions and their effect on humanoid robot 
%   Action  |           Effect
%   --------|------------------------------- 
%      1    | Move right lef up or down      
%      2    | Move right leg back or forward 
%      3    | Move left leg up or down 
%      4    | Move left leg back or forward 


% Task 1: Define a state-action reward 
reward = [[ 0 -1  0 -1];  %  1. left down_back  /  right down_back 
          [ 0  0 -1 -1];  %  2. left down_back  /  right up_back
          [ 0  0 -1 -1];  %  3. left down_back  /  right up_front 
          [ 0  0  0  0];  %  4. left down_back  /  right down_front 
          [-1 -1  0  0];  %  5. left up_back    /  right down_back
          [ 0  0  0  0];  %  6. left up_back    /  right up_back
          [ 0  0  0  0];  %  7. left up_back    /  right up_front
          [-1  1  0  0];  %  8. left up_back    /  right down_front    
          [-1 -1  0  0];  %  9. left up_front   /  right down_back
          [ 0  0  0  0];  % 10. left up_front   /  right up_back
          [ 0  0  0  0];  % 11. left up_front   /  right up_front
          [-1  1  0  0];  % 12. left up_front   /  right down_front     
          [ 0  0  0  0];  % 13. left down_front /  right down_back
          [ 0  0 -1  1];  % 14. left down_front /  right up_back
          [ 0  0 -1  1];  % 15. left down_front /  right up_front
          [-1  0 -1  0]   % 16. left down_front /  right down_front
          ];

% Task 2: Applying policy iteration 
state_transistions = [[ 2  4  5 13];
                      [ 1  3  6 14];
                      [ 4  2  7 15];
                      [ 3  1  8 16];
                      [ 6  8  1  9];
                      [ 5  7  2 10];
                      [ 8  6  3 11];
                      [ 7  5  4 12];
                      [10 12 13  5];
                      [ 9 11 14  6];
                      [12 10 15  7];
                      [11  9 16  8];
                      [14 16  9  1];
                      [13 15 10  2];
                      [16 14 11  3];
                      [15 13 12  4]
                      ];
                          
% Choose Discount Factor gamma
gamma = 0.4; 
% init random Policy 
policy = ceil(rand(16,1)*4);
policy_old = zeros(16,1); 
iter = 0;
while 1
    iter = iter+1;
    % Policiy Evaluation 
    % Calculate Bellmann equation with linear Equation 
    P_pi = zeros(16,16); 
    for state = 1:16
        % State transition propabilities for actual policy 
        P_pi(state, state_transistions(state, policy(state))) = 1;
        % get the rewards for the actual policy 
        policy_reward(state) = reward(state,policy(state));
    end 
    v_pi = inv(eye(16)-gamma*P_pi) * policy_reward'; 
    
    % Policiy Improvement 
    % Greedily update the Policy using the current value function 
    for state = 1:16
        val_old = 0; 
        for a = 1:4
            val = reward(state,a)+gamma*v_pi(state_transistions(state,a));
            if val > val_old 
                policy(state) = a; 
                val_old = val; 
            end 
        end 
    end 
    
    % Stop if policy equals old_policy 
    if all(policy_old == policy) || (iter > 1000)
        break;
    end 
    policy_old = policy; 
end

% Verification: get a State sequence with the claculated policy, starting
% in the given inital state s 
state_seq(1) = s;
for i=2:16
    state_seq(i) = state_transistions(state_seq(i-1),policy(state_seq(i-1)));
end 

walkshow(state_seq)
title(sprintf("Learned policy with policy iteration, inital state: %d", s));

