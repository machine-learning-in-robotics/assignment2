% Exercise 1: Learning dataset using Gaussian mixture model.
%
% The script loads the dataset "dataGmm.mat", initialezes the parameters of
% the GMM with the k-means algorithm und uses the Expection-Maximization
% Algorithm for the estimtation of the GMM paramerters. The number of
% components used in the GMM is equal to 4. 
% Finally a plot of the density values for inputs arranged in a grid of
% 100x100 with values in the range [-0.1, 0.1] for each variable is
% generated. 

clc
close all 
clear all 

load("dataGMM.mat")
% Define number of Components 
K = 4; 

% init GMM parameters with K-means Clustering 
[labels, code_vec] = kmeans(Data',K); 
%plot_points(Data', labels, code_vec, "K-Means")
mu = code_vec';
for i = 1:K
    sigma(:,:,i) = cov(Data(:,labels == i)');
    priors(i) = length(labels(labels==i));
end 
priors = priors/length(labels); 

% Perform Expectation-Maximization estimation 
% Define Some Parameters
convergence_threshold = 1e-10;
[dim, n] = size(Data);
convergence = inf;
log_like_old = inf; 

while convergence >= convergence_threshold
    %%%% E-STEP %%%% 
    % Compute claas wise Liklihood p(x|w,theta)
    for i = 1:K
        p_xw(:, i) = gauss_pdf(Data, mu(:,i), sigma(:,:,i));
    end 
    % Compute posterior probability p(w|x, theat)
    for j = 1:n 
        p_wx(j,:) = priors.*p_xw(j,:) / sum(priors.*p_xw(j, :));
    end 
    
    %%%% M-STEP %%%% 
    % Compute cumulated posterior probability
    n_k = sum(p_wx);
    for i = 1:K      
        % Update Priors 
        priors(i) = n_k(i) / n; 
        % Update Mean Values 
        mu(:,i) = Data*p_wx(:,i) / n_k(i);
        % Update Covariance 
        temp = Data-repmat(mu(:,i),1,n);
        sigma(:,:,i) = (repmat(p_wx(:,i)', dim, 1) .* temp * temp')/n_k(i);
    end 
    
    %%% Evaluation %%% 
    % Compute new likelihood p(x|w,theta_new) 
    for i = 1:K
        p_xw(:, i) = gauss_pdf(Data, mu(:,i), sigma(:,:,i));
    end 
    % Compute log-likelihood
    log_like = sum(log(p_xw*priors'));
    
    convergence = abs((log_like/log_like_old)-1);
    log_like_old = log_like;
end

% Display final Values 
priors
mu
sigma

% Plot GMM 
plot_density(mu, sigma, priors, Data)


%%%%%%%%%%%%%% Helper Functions %%%%%%%%%%%%%%%%%%%%

function prob = gauss_pdf(x, mu, sigma)
% Calculates the pdf of multivariate gaussian for multiple input data x 
% x: DxN array of N datapoints with dimension D
% mu: Dx1 mean value with dimenson D for one GMM component  
% simga: DxD covariance matrix for one GMM component
% prob: Nx1 probabilities for datapoints
[dim, n] = size(x);
x = x'-repmat(mu',n, 1); 
prob = sum((x/sigma).*x, 2); 
prob = exp(-0.5*prob)/sqrt((2*pi)^dim*det(sigma));
end 

function plot_density(mu, sigma, priors, data)

x1 = -0.1:0.002:0.1;
x2 = -0.1:0.002:0.1;
[X1,X2] = meshgrid(x1,x2);
X = [X1(:) X2(:)];

for i = 1:size(sigma,3)
    y(i,:) = priors(i)*mvnpdf(X,mu(:,i)',sigma(:,:,i));
end

y = sum(y, 1);
y = reshape(y,length(x2),length(x1));
s = surf(x1,x2,y,'FaceAlpha',0.8);
hold on;
h1 = scatter3(data(1,:)',data(2,:)',zeros(size(data,2),1), 'black');
h1.MarkerFaceColor = 'black';

title('Exercise1: 3D - Density Surface Plot of GMM with underlaid Data Points')
xlabel('x1')
ylabel('x2')
zlabel('Probability Density')
view(2);

end


