function [newstate, reward] = SimulateRobot(state, action)

r = [[ 0 -1  0 -1];  %  1. left down_back  /  right down_back 
     [ 0  0 -1 -1];  %  2. left down_back  /  right up_back
     [ 0  0 -1 -1];  %  3. left down_back  /  right up_front 
     [ 0  0  0  0];  %  4. left down_back  /  right down_front 
     [-1 -1  0  0];  %  5. left up_back    /  right down_back
     [ 0  0  0  0];  %  6. left up_back    /  right up_back
     [ 0  0  0  0];  %  7. left up_back    /  right up_front
     [-1  1  0  0];  %  8. left up_back    /  right down_front    
     [-1 -1  0  0];  %  9. left up_front   /  right down_back
     [ 0  0  0  0];  % 10. left up_front   /  right up_back
     [ 0  0  0  0];  % 11. left up_front   /  right up_front
     [-1  1  0  0];  % 12. left up_front   /  right down_front     
     [ 0  0  0  0];  % 13. left down_front /  right down_back
     [ 0  0 -1  1];  % 14. left down_front /  right up_back
     [ 0  0 -1  1];  % 15. left down_front /  right up_front
     [-1  0 -1  0]   % 16. left down_front /  right down_front
     ];

% Task 2: Applying policy iteration 
state_transistions = [[ 2  4  5 13];
                      [ 1  3  6 14];
                      [ 4  2  7 15];
                      [ 3  1  8 16];
                      [ 6  8  1  9];
                      [ 5  7  2 10];
                      [ 8  6  3 11];
                      [ 7  5  4 12];
                      [10 12 13  5];
                      [ 9 11 14  6];
                      [12 10 15  7];
                      [11  9 16  8];
                      [14 16  9  1];
                      [13 15 10  2];
                      [16 14 11  3];
                      [15 13 12  4]
                      ];

newstate = state_transistions(state, action);
reward = r(state, action);

end
