% Exercise 2: Human gesture recognition using hidden Markov model
% Classifiey the given data in Test.txt into gesture 1 or gesture 2. 
% The numer of state in the HMM is N=12 and the number of discrete
% observations in each state is M=8 

clc
close all
clear all 

% Import the given Data
test = load('Test.txt');
A = load('A.txt');
B = load('B.txt');
% Transpose B to be NxM
B = B'; 
pi = load('pi.txt');

[T, nObsSequ] = size(test);
[N, ~] = size(A);
[M, ~] = size(B);

% Use the Forward Algorithm to determine the probability p(O|lamda) that a
% sequence O={o1,o2 ... oT} of visible states was generated by the model 
% lamda={A,B,pi}

for s = 1 : nObsSequ
    seq = test(:, s);
    
    % init Forward Variable 
    for i = 1 : N 
        alpha(1, i) = pi(i)*B(i, seq(1));
    end 
    % induction 
    for t = 1 : (T-1) 
        for j = 1 : N 
            alpha(t+1, j) = alpha(t,:) * A(:,j) * B(j, seq(t+1));
        end
    end   
    % Termination
    like  = sum(alpha(T,:));
    % calculate log-likelihood for each sequence 
    log_like(s) = log(like); 
end

% Classificaiton of each sequence reagarding to the given Classification
% criterion: 
%
% gesture1: if log-likelihood>-115
% gesture2: oterwise 

for i = 1:length(log_like)
    if log_like > -115
        class_results(i) = 1;
    else
        class_results(i) = 2;
    end 
end 

% Create Cool Output 
format = repmat('%-40s',1,11);
header = sprintf(format,'Sequence  |  Gesture  |  Log-Likelihood');
fprintf('\n%s\n%s\n',header,repmat('-',size(header)));
for i = 1:length(log_like)
    fprintf("%5d %12d %15.2f", i, class_results(i), log_like(i));
    fprintf('\n')
end 



